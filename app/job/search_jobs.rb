class SearchJobs
  attr_accessor :client
  #This method is a Constructor.
  #This method create the instance variables in the class, those can be used in all methods.
  def initialize(client)
    @client = client
  end

  def perform
    params ={'q' => 'python', 'title' => 'Web Developer'}
    api_response = client.get '/profiles/v2/search/jobs', params
    jobs_array = Array.new
    api_response['jobs'].each do |job|
      upwork_job = UpworkJob.find_or_initialize_by(upwork_job_id: job['id'])
      if upwork_job.new_record?
        jobs_array << job
        upwork_job.save
      end
    end
    auth_user = api_response['auth_user']['first_name']
    unless jobs_array.empty?
      UserMailer.delay.job_email(jobs_array, auth_user)
    end
  end

  #This method is called if all the data is successfully fetched for a company blog.
  #DISTRIBUTOR_STATUS_URL is defined in "config/initializers/blog_config.rb".
  #This method posts the success status xml on DISTRIBUTOR_ STATUS_URL on success.

  def success(job)
    # Delayed::Job.enqueue SearchJobs.new(@client), 0, 30.seconds.from_now.getutc
  end

  #This method is called if there is any error while the data extraction process from blog.
  #DISTRIBUTOR_STATUS_URL is defined in "config/initializers/blog_config.rb".
  #This method posts the failure status xml on DISTRIBUTOR_STATUS_URL on failure.
  def error(job, exception)
    UserMailer.delay.exception_mail(exception)
  end

  #This method is called if there is failure while the data extraction process from blog.
  #DISTRIBUTOR_STATUS_URL is defined in "config/initializers/blog_config.rb".
  #This method posts the failure status xml on DISTRIBUTOR_STATUS_URL on failure.
  def failure
  end

end