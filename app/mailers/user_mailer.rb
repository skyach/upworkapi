class UserMailer < ActionMailer::Base
  default from: "rahul.ranchal@skyach.com"

 #following method used to send  jobs mail

  def job_email(jobs_array,user_name)
    @jobs_array  = jobs_array
    @user_name = user_name
    mail(to: "rahul.ranchal@skyach.com", subject: 'Jobs')
    send_sms(jobs_array)
  end

 #following method used to send exception mail

  def exception_mail(error)
    @error = error
    mail(to: "rahul.ranchal@skyach.com", subject: 'Jobs')
  end



  private

  # following method used to send sms of job
  def send_sms(jobs_array)
    jobs_array.each do |job|
      @url = job["url"]
      @budget = job["budget"]
      @duration = job["duration"]
      if (!@duration.nil?)
        message = "jobs #{@url}, Budget #{@budget}, Duration #{@duration}"
      else
        message = "jobs #{@url}, Budget #{@budget}"
      end
      api = Msg91ruby::API.new("109028Ad8JyM4Ww56ff91c7","SKYACH")
      api.send(9779799889, message, 4)
    end
  end
end
