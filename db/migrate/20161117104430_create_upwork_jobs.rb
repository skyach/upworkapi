class CreateUpworkJobs < ActiveRecord::Migration
  def change
    create_table :upwork_jobs do |t|
      t.string :upwork_job_id

      t.timestamps
    end
  end
end
